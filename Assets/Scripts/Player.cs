﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    Rigidbody2D rigidbody;
    float speed = 30;
    float maxSpeed = 7;

    float jumpStrength = 300;

    bool isGrounded = true;

    // Use this for initialization
    void Start () {
		rigidbody = gameObject.GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) && isGrounded)
        {
            rigidbody.AddForce(Vector2.up * jumpStrength);
            isGrounded = false;
        }

        if (Input.GetKey(KeyCode.S))
        {

        }

        if (Input.GetKey(KeyCode.A) && rigidbody.velocity.x > -maxSpeed)
        {
            rigidbody.AddForce(Vector2.left * speed);
        }

        if (Input.GetKey(KeyCode.D) && rigidbody.velocity.x < maxSpeed)
        {
            rigidbody.AddForce(Vector2.right * speed);
        }
    }

    void OnCollisionEnter2D (Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Goal"))
        {
            Debug.Log("You win!");
        }
    }
}
